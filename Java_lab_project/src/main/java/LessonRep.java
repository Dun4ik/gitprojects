import models.Lesson;

import java.util.Optional;

public interface LessonRep {
    Optional<Lesson> findById(Integer id);
    Optional<Lesson> findByName(String name);
    void update(Lesson lesson);
    void save(Lesson lesson, Integer CoursesID);
}
