import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import models.Course;
import models.Lesson;
import models.Teacher;

import javax.sql.DataSource;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileReader("src//main//resources//application.properties"));

        HikariConfig config = new HikariConfig();
        config.setDriverClassName(properties.getProperty("db.driver"));
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.user"));
        config.setPassword(properties.getProperty("db.password"));


        DataSource dataSource = new HikariDataSource(config);
        CourseRep courseRep = new CourseRepJdbcTemplateImpl(dataSource);

        System.out.println(courseRep.findById(1) + "\n");


        Teacher teacher = new Teacher("Александр", "Лицкеевич", "7");
        Course course = new Course("UnityStart", "example", "example");
        courseRep.save(course, teacher);

        Course courseMathematics = courseRep.findById(3).orElse(new Course());
        courseMathematics.setName("Discrete Math");
        courseMathematics.setDateStart("1.09.21");
        courseMathematics.setDateEnd("29.05.21");
        courseRep.update(courseMathematics);

        System.out.println(courseRep.findByName("Digital Media Lab") + "\n");

        LessonRep lessonRep = new LessonRepJdbcTemplateImpl(dataSource);
        System.out.println(lessonRep.findById(1) + "\n");

        Lesson lesson = new Lesson("Graph", "Monday", "13:00");
        lessonRep.save(lesson, 5);

        Lesson lesson1 = lessonRep.findById(1).orElse(new Lesson());
        lesson1.setName("Matrices");
        lesson1.setDayWeek("Monday");
        lesson1.setTime("8:00");
        lessonRep.update(lesson1);

        System.out.println(lessonRep.findByName("Matrices") + "\n");
    }
}
