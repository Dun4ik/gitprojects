insert into course(name, date_start, data_end)
values ('JavaLab', '07.07.2021', '7.08.2021');
insert into course(name, date_start, data_end)
values ('Simbirsoft', '23.06.2021', '18.07.2021');
insert into course(name, date_start, data_end)
values ('Android development', '01.06.2021', '01.07.2021');
insert into course(name, date_start, data_end)
values ('Digital Media Lab', '03.07.2021', '17.07.2021');


insert into lesson(name, day_week, time)
values ('SQL', 'Monday', '14:00');
insert into lesson(name, day_week, time)
values ('JDBC', 'Friday', '14:30');
insert into lesson(name, day_week, time)
values ('Project Management', 'Tuesday', '12:00');
insert into lesson(name, day_week, time)
values ('Unity', 'Saturday', '10:00');
insert into lesson(name, day_week, time)
values ('Kotlin', 'Monday', '11:00');
insert into lesson(name, day_week, time)
values ('Unreal Engine', 'Monday', '11:00');
insert into lesson(name, day_week, time)
values ('Maven', 'Thursday', '14:00');
insert into lesson(name, day_week, time)
values ('Project', 'Saturday', '15:00');


insert into lesson_courses_relation(lesson_id, course_id)
values (1, 1);
insert into lesson_courses_relation(lesson_id, course_id)
values (2, 1);
insert into lesson_courses_relation(lesson_id, course_id)
values (3, 2);
insert into lesson_courses_relation(lesson_id, course_id)
values (5, 3);
insert into lesson_courses_relation(lesson_id, course_id)
values (6, 4);
insert into lesson_courses_relation(lesson_id, course_id)
values (8, 2);


insert into teacher(first_name, last_name, experience)
values ('Марсель', 'Сидиков', '10');
insert into teacher(first_name, last_name, experience)
values ('Ирина', 'Шахова', '5');
insert into teacher(first_name, last_name, experience)
values ('Сахипбагареева', 'Гульнара', '6');


insert into teacher_courses_relation(teacher_id, course_id)
values (1, 1);
insert into teacher_courses_relation(teacher_id, course_id)
values (2, 3);
insert into teacher_courses_relation(teacher_id, course_id)
values (3, 4);



insert into student(first_name, last_name, number_group)
values ('Вадим', 'Поляков', '11-005');
insert into student(first_name, last_name, number_group)
values ('Карина', 'Обшарова', '11-005');
insert into student(first_name, last_name, number_group)
values ('Александра', 'Сафина', '11-002');
insert into student(first_name, last_name, number_group)
values ('Анастасия', 'Войтенко', '11-004');
insert into student(first_name, last_name, number_group)
values ('Диляра', 'Кошапова', '11-001');
insert into student(first_name, last_name, number_group)
values ('Родион', 'Каримов', '11-005');
insert into student(first_name, last_name, number_group)
values ('Миргалиев', 'Тимур', '11-005');
insert into student(first_name, last_name, number_group)
values ('Егор', 'Лозовой', '11-005');
insert into student(first_name, last_name, number_group)
values ('Адель', 'Валеев', '11-001');
insert into student(first_name, last_name, number_group)
values ('Денис', 'Алферов', '11-007');
insert into student(first_name, last_name, number_group)
values ('Сивачев', 'Никита', '11-006');
insert into student(first_name, last_name, number_group)
values ('Эмиль', 'Мифтахов', '11-005');

insert into students_courses_relation(stud_id, course_id)
values (1, 2);
insert into students_courses_relation(stud_id, course_id)
values (1, 3);
insert into students_courses_relation(stud_id, course_id)
values (2, 1);
insert into students_courses_relation(stud_id, course_id)
values (2, 2);
insert into students_courses_relation(stud_id, course_id)
values (3, 1);
insert into students_courses_relation(stud_id, course_id)
values (4, 4);
insert into students_courses_relation(stud_id, course_id)
values (5, 4);
insert into students_courses_relation(stud_id, course_id)
values (6, 3);
insert into students_courses_relation(stud_id, course_id)
values (7, 3);
insert into students_courses_relation(stud_id, course_id)
values (8, 2);
insert into students_courses_relation(stud_id, course_id)
values (9, 1);
insert into students_courses_relation(stud_id, course_id)
values (10, 4);
insert into students_courses_relation(stud_id, course_id)
values (11, 2);
insert into students_courses_relation(stud_id, course_id)
values (12, 1);
insert into students_courses_relation(stud_id, course_id)
values (12, 4);