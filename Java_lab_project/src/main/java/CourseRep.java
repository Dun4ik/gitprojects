import models.Course;
import models.Teacher;
import java.util.Optional;

public interface CourseRep {
    Optional<Course> findById(Integer id);
    Optional<Course> findByName(String name);
    void update(Course course);
    void save(Course course, Teacher teacher);
}